<?php

namespace Drupal\map_to_view\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Entity\View;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Plugin implementation of the 'Random_default' formatter.
 *
 * @FieldFormatter(
 *   id = "map_to_view",
 *   label = @Translation("Map to View"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class MapToViewFieldFormatter extends FormatterBase {

  public function settingsSummary(){
    $summary = [];
    $summary[] = $this->t('Displays a view using the field values as a contextual filter.');
    return $summary;
  }

  /**
   * @inerhitDoc
   */
  public function viewElements(FieldItemListInterface $items, $langcode){
    $element = [];

    // Load the view that was selected in field display configuration.
    $selected_view = $this->getSetting('selected_view');
    $additional_args = $this->getSetting('additional_args');
    $split = explode(':', $selected_view);

    $view = Views::getView($split[0]);
    if($view instanceof ViewExecutable){
      $view->setDisplay($split[1]);

      // Retrieve config for the arguments.
      $arguments_config = $view->getDisplay()->getOption('arguments');

      $vals = [];
      foreach($items as $item) {
        $vals[] = $item->getValue()['target_id'];
      }

      if(count($vals) > 1){
        // Assume that we want to have an OR relationship between conditions.
        // If joined by a comma then it would be an AND Relationship.
        $args[0] = implode('+', $vals);
      }elseif(count($vals) === 1){
        $args[0] = array_shift($vals);
      }else{
        // No filter values provided, pass the exception value from the views config.
        $arg_info = array_shift($arguments_config);
        if(!empty($arg_info['exception']['value'])){
          $args = [0 => $arg_info['exception']['value']];
        }else{
          // Pass the default.
          $args = [0 => 'all'];
        }
      }

      if(!empty($additional_args)){
        $entity = $items->getEntity();
        if($entity->hasField($additional_args)){
          $added_values = $entity->get($additional_args)->getValue();
          if(count($added_values)){
            $avs = array_column($added_values, 'target_id');
            if(count($avs) > 1){
              // Assume that we want to have an OR relationship between conditions.
              // If joined by a comma then it would be an AND Relationship.
              $args[1] = implode('+', $avs);
            }elseif(count($avs) === 1){
              $args[1] = array_shift($avs);
            }else{
              // No filter values provided, pass the exception value from the views config.
              $arg_info = array_pop($arguments_config);
              if(!empty($arg_info['exception']['value'])){
                $args = [0 => $arg_info['exception']['value']];
              }else{
                // Pass the default.
                $args = [0 => 'all'];
              }
            }
          }
        }
      }

      $view->setArguments($args);
      $view->preExecute();
      $view->execute();
      $element = $view->buildRenderable($split[1], $args);
    }

    return $element;
  }

  public static function defaultSettings(){
    return [
      'selected_view' => '',
      'additional_args' => NULL
    ];
  }

  /**
   * @InheritDoc
   */
  public function settingsForm(array $form, FormStateInterface $form_state){
    $options = Views::getViewsAsOptions(FALSE, 'enabled');

    $form['selected_view'] = [
      '#title' => $this->t('Views'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->getSetting('selected_view'),
    ];


    $entity_type_id = $this->fieldDefinition->getTargetEntityTypeId();
    $bundle_id = $this->fieldDefinition->getTargetBundle();
    $fields = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions($entity_type_id, $bundle_id);

    $field_options = [0 => $this->t('None')];

    $valid_types = [
      'entity_reference'
    ];

    $this_field_name = $this->fieldDefinition->getName();

    foreach($fields as $field) {
      if($field->getName() != $this_field_name &&
        in_array($field->getType(), $valid_types)){
        $field_options[$field->getName()] = $field->getLabel();
      }
    }

    $form['additional_args'] = [
      '#type' => 'select',
      '#title' => $this->t('Select a Field for an additional arguments.'),
      '#options' => $field_options,
      '#default_value' => $this->getSetting('additional_args'),
      '#description' => $this->t('Only entity reference fields are supported at this time.'),
    ];

    return $form;
  }

}
